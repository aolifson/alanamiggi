﻿using System.Web.Mvc;
using AmiggiEncrypt;

namespace encrypt.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index()
    {
      var corpId = "corpId=" + Request.QueryString["corpId"];
      var crisId = "crisId=" + Request.QueryString["crisId"];
      string encrypted = Security.Encrypt(corpId + "&" + crisId);

      return View("Index",model:encrypted);
    }

    
  }
}