﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace AmiggiEncrypt
{
  public class Security
  {
    /**
     * Encrypts a string using AesCryptoServiceProvider
     */
    public static string Encrypt(string raw)
    {
      using (var csp = new AesCryptoServiceProvider())
      {
        var e = GetCryptoTransform(csp, true);
        byte[] inputBuffer = Encoding.UTF8.GetBytes(raw);
        byte[] output = e.TransformFinalBlock(inputBuffer, 0, inputBuffer.Length);

        var encrypted = Convert.ToBase64String(output);

        return encrypted;
      }
    }

   
    private static ICryptoTransform GetCryptoTransform(AesCryptoServiceProvider csp, bool encrypting)
    {
      csp.Mode = CipherMode.CBC;
      csp.Padding = PaddingMode.PKCS7;
      // Must be same strings below as we use in the Java decryption class
      // we can move this to a config or property file if needed
        var passWord = "OurPasswordHere";
        var salt = "OurSaltHere";
        //a random initialization. 
        String iv = "e675f725e675f725";

      var spec = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(passWord), Encoding.UTF8.GetBytes(salt), 65536);
      byte[] key = spec.GetBytes(16);


      csp.IV = Encoding.UTF8.GetBytes(iv);
      csp.Key = key;
      if (encrypting)
      {
        return csp.CreateEncryptor();
      }
      return csp.CreateDecryptor();
    }
  }
 
}
