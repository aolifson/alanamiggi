import com.sun.crypto.provider.SunJCE;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.spec.KeySpec;

public class decrpt {

    public static void main(String[] args) throws Exception {
           String data = args[0];

        String decrypt = Decrypt(data);
        String[]params = decrypt.split("&");
        String corpId = params[0].split("corpId=")[1];
        String crisId = params[1].split("crisId=")[1];

        // to do - do something with the decrypted values
        System.out.println(decrypt);
        System.out.println(corpId + " / " + crisId);
    }


    private static Cipher getCipher(int mode) throws Exception {
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding", new SunJCE());
        // random init set in .net encryption code
        byte[] iv = "e675f725e675f725".getBytes("UTF-8");

        c.init(mode, generateKey(), new IvParameterSpec(iv));
        return c;
    }

    private static String Decrypt(String encrypted) throws Exception {

        byte[] decodedValue = new BASE64Decoder().decodeBuffer(encrypted);

        Cipher c = getCipher(Cipher.DECRYPT_MODE);
        byte[] decValue = c.doFinal(decodedValue);

        return new String(decValue);
    }

    private static Key generateKey() throws Exception {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        // same password and salt  as set in .net encryption code. Can store these in shared config /prop files
        char[] password = "OurPasswordHere".toCharArray();
        byte[] salt = "OurSaltHere".getBytes("UTF-8");

        KeySpec spec = new PBEKeySpec(password, salt, 65536, 128);
        SecretKey tmp = factory.generateSecret(spec);
        byte[] encoded = tmp.getEncoded();
        return new SecretKeySpec(encoded, "AES");

    }
}